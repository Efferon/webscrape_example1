**Project Overview**

Simple webscraping demonstration using a python script with the 
[Beautiful Soup V4 library](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) 

The project scrapes speaker and presenter information from the 
[2018  Neural Information Processing Systems conference](https://nips.cc/Conferences/2018/Schedule)

![picture](img/WebscrapeExample1.png)


The client wanted to get the following data:

- Presentation Title
- Presentation Type (i.e. poster, paper)
- Author/Speaker names and their affiliation
- Presenation Topic (if available) *(e.g., Clustering, Reinforcement Learning, Deep Learning)*
- Email (if available)
- Website (if available)
- Abstract

- The following was also available but not requested
-   Resource Links *(paper home page, videos, slides)*


**Webscraping Design** 

1. From [starting page](https://nips.cc/Conferences/2018/Schedule) get all event IDs
1. For each event ID, *except breaks*, get the event information, and
1. Follow each author ID to get author information

**Comments**
While building the system, you often need to process the same file; to be a polite net-citizen, the data should be 
cached. This is accomplished using the *getWebpageOrUrlCache(url, cached_url_filename)* function.

If your not familiar with Beautiful Soup, then you might find it difficult to understand that and HTML or XML tree
can be processeed; often you pass subtrees for further processing. Many people just call this "soup" no matter if the 
whole HTLM document is passed or just a subtree.  I like to name my functions to that its easier to see what piece of
data is being extracted; additionally if the website should be changed its easier to focus on just the snippet needs 
to be updated rather than having to trace the entire scraping program.

