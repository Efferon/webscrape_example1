import re
import urllib.request
from bs4 import BeautifulSoup

from pathlib import Path


# Summary of Scraping
# Not Available -->
#   Presenation Topic  - (e.g., Clustering, Reinforcement Learning, Deep Learning)
#   Email (if available)
#   Website (if available)
#
# Available -->
#   Author/Speaker names and affiliation
#   Title
#   Abstract
#   Type (i.e. poster, paper)
#
# Available but not requested -->
#   Resource Links



# starting page for website conference data
starting_url = 'https://nips.cc/Conferences/2018/Schedule'

# Example card with multiple authors Andrey Malinin and Mark Gales
# 'https://nips.cc/Conferences/2018/Schedule?showEvent=11679'

# Example author page Andrey Malinin University of Cambridge
# 'https://nips.cc/Conferences/2018/Schedule?showSpeaker=53532-11679'

# website conference data cached copy
cached_url_filename = 'cache/NIPS 2018.html'


def getWebpageOrCache(url, cached_url_filename):
    myfile = Path(cached_url_filename)
    if myfile.is_file():
        with open(cached_url_filename) as f:
            return f.read() # returning cached file
    else:
        f1 = urllib.request.urlopen(url)
        webpage = f1.read()
        # Note urlopen returns bytes not string
        with open(cached_url_filename, 'wb') as f2:
            return f2.write(webpage)


def getSpeakerID(eventPageSoup):
    # A tag of:
    # ---- <button onclick="showSpeaker('50332-10984');" class="btn btn-default" style="margin:5px">
    #      <span class="glyphicon glyphicon-user"></span> David Dunson »</button>
    # will return '50332-10984'
    speaker_id_pattern = r"\d+-\d+"
    showspeaker = eventPageSoup['onclick']
    id = re.search(speaker_id_pattern, showspeaker)
    return id.group()  # May return NONE if tag doesn't contain speaker id


def getEventIDFromEventTag(eventPageSoup):
    # A tag of:
    # ---- <div onclick="showDetail(12867)">
    # will return '12867'
    eventIdPattern = r"\d+"
    showevent = eventPageSoup['onclick']
    id = re.search(eventIdPattern, showevent)
    return id.group()  #May return NONE if tag doesn't contain speaker id


def getSpeakerIDsFrom(eventPageSoup):
    # The soup should generally a page that 'showsEvent' as in


    tags =  eventPageSoup.find_all(onclick=re.compile("^showSpeaker"))

    speakerIDs = []
    for tag in tags:
        speakerIDs.append(getSpeakerID(tag))
    return speakerIDs


def getSpeakerInfoFor(speakerIDs):
    speakers = []
    for id in speakerIDs:
        url = 'https://nips.cc/Conferences/2018/Schedule?showSpeaker=' + id
        f1 = urllib.request.urlopen(url)
        page = f1.read()
        soup = BeautifulSoup(page, features="html.parser")

        speaker_name = soup.find("div", {"class": "maincard"}).find("h3").string
        affiliation = soup.find("div", {"class": "maincard"}).find("h4").string

        speakers.append({speaker_name: affiliation})
    return speakers


def getEventInformationFor(eventID):
    # SHOWEVENT PAGES have the following information
    #     eg: https://nips.cc/Conferences/2018/Schedule?showEvent=11679
    #
    #  Time/Date/Room "Tue Dec 4th 10:45 AM -- 12:45 PM @ Room 517 AB #106"  --NOT NEEDED
    #  Type of Event "Poster"
    #  Name  "Predictive Uncertainty Estimation via Prior Networks"
    #  ShowAuthor links - "Andrey Malinin, Andrey Malinin"
    #  Links to Resources
    #  Category "Tue Poster Session A"  --NOT NEEDED
    #  Abstract

    eventInfo = {}
    url = 'https://nips.cc/Conferences/2018/Schedule?showEvent=' + eventID
    f1 = urllib.request.urlopen(url)
    page = f1.read()
    soup = BeautifulSoup(page, features="html.parser")

    # Note this finds with two classes
    presentation_type = soup.find("div", {"class": "maincardHeader"}, {"class":"maincardType"})
    presentation_title = soup.find("div", {"class": "maincardBody"})
    abstract = soup.find("div", {"class": "abstractContainer"})

    speakerIDs = getSpeakerIDsFrom(soup)
    speakerInfo = getSpeakerInfoFor(speakerIDs)

    return {"type" : presentation_type.string,
            "speakers" : speakerInfo,
            "title" : presentation_title.string,
            "abstract" : abstract.contents}


def getEventIDsFrom(url):
    page = getWebpageOrCache(url, cached_url_filename )

    soup = BeautifulSoup(page, features="html.parser")

    tags =  soup.find_all(onclick=re.compile("^showDetail"))

    eventIDs = []
    for tag in tags:
        eventIDs.append(getEventIDFromEventTag(tag))
    return eventIDs


def getAllInfo(starting_url):
    for getEventPage in getEventIDsFrom(starting_url):
        # Note event id ["12867"] is an example of a Break]]

        print(getEventPage)

        if getEventInformationFor(getEventPage)["type"] != "Break":
            print(getEventInformationFor(getEventPage))


getAllInfo(starting_url)







